# Quelldokumente

- Transkriptionen der Handschriften (PDF / TXT)
    - DE-14: [Sächsische Landesbibliothek – Staats- und Universitätsbibliothek](https://nbn-resolving.org/urn:nbn:de:bsz:14-qucosa-106475)
- Gesamtinhaltsverzeichnis der Print-Ausgabe (PDF / TXT)
    - DE-101: [Deutsche Nationalbibliothek](https://d-nb.info/1186990775/04)
