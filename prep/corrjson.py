#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
script to extract correspondence metadata
from plain text files found at:

  ../docs/DE-14/Qucosa/*.txt
  ../docs/DE-101/1186990775/toc/bd[123].txt

Creator:           D. Herre
GitLab:     dikon/woma-cmif

Created:         2020-06-18
Last Modified:   2020-06-19
"""

import os
import re
import json

BASE = "docs/DE-101/1186990775/toc"
BAND_1 = os.path.join(BASE, "bd1.txt")
BAND_2 = os.path.join(BASE, "bd2.txt")
BAND_3 = os.path.join(BASE, "bd3.txt")

BASE_2 = "docs/DE-14/Qucosa"
TRANS_1 = os.path.join(BASE_2, "Wolff-Manteuffel_Transkriptionen_Briefe_001-150.txt")
TRANS_2 = os.path.join(BASE_2, "Wolff-Manteuffel_Transkriptionen_Briefe_151-314.txt")
TRANS_3 = os.path.join(BASE_2, "Wolff-Manteuffel_Transkriptionen_Briefe_315-488.txt")

index_pattern = re.compile("^(\\d+)\\.\\s")


def read_plain(path):
    with open(path, 'r', encoding="utf-8") as f:
        return [l.strip() for l in f.readlines()]


bd_1 = read_plain(BAND_1)
bd_2 = read_plain(BAND_2)
bd_3 = read_plain(BAND_3)

trans_1 = read_plain(TRANS_1)
trans_2 = read_plain(TRANS_2)
trans_3 = read_plain(TRANS_3)


def extract(lines):
    """extract correspondence metadata from plain text toc of print edition"""
    bd_data = {"letter": {}}
    for line in lines:
        # print(line)
        components = line.split(", ")
        index = index_pattern.search(components[0]).groups()[0]
        letter = {}
        correspondents = index_pattern.sub("", components[0])
        correspondents = correspondents.split(" an ")
        letter['author'] = correspondents[0]
        letter['addressee'] = correspondents[1]
        letter['place'] = components[1]
        letter['date'] = components[2]
        bd_data["letter"][index] = letter
    return bd_data


def extract2(lines):
    """extract correspondence metadata from plain text transcripts"""
    bd_data = {"letter": {}}
    for i, line in enumerate(lines):
        if "WOLFF AN MANTEUFFEL" in line or \
          "MANTEUFFEL AN WOLFF" in line:
            index = lines[i-2].replace("NR. ", "")
            correspondents = line.split(" AN ")
            place_date = lines[i+1] if lines[i+1] != "" \
                else lines[i+2]
            letter = {}
            letter['author'] = correspondents[0]
            letter['addressee'] = correspondents[1]
            letter['place'] = [p.strip() for p in place_date.split(",")][0]
            letter['date'] = [d.strip() for d in place_date.split(",")][1]
            bd_data["letter"][index] = letter
        else:
            continue
    return bd_data


def main():
    """main routine to extract correspondence metadata from plain text"""
    data_print = {"volume": {}}
    for i, lines in enumerate([bd_1, bd_2, bd_3]):
        data_print["volume"][i+1] = extract(lines)
    s_print = json.dumps(data_print, ensure_ascii=False, indent=2)
    with open('data/briefverzeichnis-print.json', 'w', encoding='utf-8') as f:
        f.write(s_print)
    data_online = {"volume": {}}
    for i, lines in enumerate([trans_1, trans_2, trans_3]):
        data_online["volume"][i+1] = extract2(lines)
    s_print = json.dumps(data_online, ensure_ascii=False, indent=2)
    with open('data/briefverzeichnis-online.json', 'w', encoding='utf-8') as f:
        f.write(s_print)


if __name__ == '__main__':
    main()
