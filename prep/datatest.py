#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
script to check correspondence metadata
from json file found at:

  ../data/briefverzeichnis.json

Creator:          D. Herre
GitLab:     dikon/woma-cmif

Created:        2020-06-18
Last Modified:  2020-06-19
"""

import re
import os
import json

metadata = {}

with open("data/briefverzeichnis-print.json", "r", encoding="utf-8") as f:
    metadata = json.load(f)


reCorrAdd = re.compile('\\[.*?\\]')
reCorrAdd2 = re.compile('\\[|\\]')


def authors_unique():
    """get list of unique authors"""
    authors = []
    for num in metadata["volume"]:
        for index in metadata["volume"][num]["letter"]:
            authors.append(metadata["volume"][num]["letter"][index]["author"])
    authors = list(set(authors))
    authors.sort()
    return authors


def addressees_unique():
    """get list of unique addressees"""
    addressee = []
    for num in metadata["volume"]:
        for index in metadata["volume"][num]["letter"]:
            addressee.append(metadata["volume"][num]["letter"][index]["addressee"])
    addressee = list(set(addressee))
    addressee.sort()
    return addressee


def persons_unique():
    """get list of unique persons (authors + addressees)"""
    authors = authors_unique()
    addressees = addressees_unique()
    persons = list(set(authors + addressees))
    persons.sort()
    return persons


def save_persons_template():
    """save list of unique persons to json template file"""
    persons = persons_unique()
    template = {}
    for e in persons:
        template[e] = ""
    if not os.path.exists("prep/datatest"):
        os.makedirs("prep/datatest")
    with open("prep/datatest/persons.json", "w+", encoding="utf-8") as f:
        s = json.dumps(template, ensure_ascii=False, indent=2)
        f.write(s)


def save_persons():
    """save list of unique entities to plain text file"""
    entities = persons_unique()
    if not os.path.exists("prep/datatest"):
        os.makedirs("prep/datatest")
    with open("prep/datatest/persons.txt", "w+", encoding="utf-8") as f:
        f.write("\n".join(entities))


def places_unique():
    """get list of unique places"""
    places = []
    for num in metadata["volume"]:
        for index in metadata["volume"][num]["letter"]:
            places.append(metadata["volume"][num]["letter"][index]["place"])
    places = list(set(places))
    places.sort()
    return places


def save_places():
    """save list of unique places to plain text file"""
    places = places_unique()
    if not os.path.exists("prep/datatest"):
        os.makedirs("prep/datatest")
    with open("prep/datatest/places.txt", "w+", encoding="utf-8") as f:
        f.write("\n".join(places))


def save_places_template():
    """save list of unique places to json template file"""
    places = places_unique()
    template = {}
    for p in places:
        template[p] = ""
    if not os.path.exists("prep/datatest"):
        os.makedirs("prep/datatest")
    with open("prep/datatest/places.json", "w", encoding="utf-8") as f:
        s = json.dumps(template, ensure_ascii=False, indent=2)
        f.write(s)


def dates_unique():
    """get list of unique dates"""
    dates = []
    for num in metadata:
        for num in metadata["volume"]:
            for index in metadata["volume"][num]["letter"]:
                dates.append(metadata["volume"][num]["letter"][index]["date"])
    dates = list(set(dates))
    dates.sort()
    return dates


def save_dates():
    """save list of unique dates to plain text file"""
    dates = dates_unique()
    if not os.path.exists("prep/datatest"):
        os.makedirs("prep/datatest")
    with open("prep/datatest/dates.txt", "w+", encoding="utf-8") as f:
        f.write("\n".join(dates))


def main():
    """routine to test JSON data"""
    save_dates()
    save_places()
    save_persons()
    save_persons_template()
    save_places_template()


if __name__ == '__main__':
    main()
