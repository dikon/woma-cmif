#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
prepare TEI-based files in CMI format for the correspondence
between Christian Wolff and Ernst Christoph von Manteuffel

Creator:          D. Herre
GitLab:    dikon/woma-cmif

Created:        2020-06-19
Last Modified:  2020-06-19
"""

from . import corrjson
from . import datatest
from . import corrprint
from . import corronline

corrjson.main()
datatest.main()
corrprint.main()
corronline.main()
