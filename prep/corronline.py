#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
script to translate correspondence metadata
to TEI-based CMI format from JSON data found at:

  ../data/briefverzeichnis-online.json

Creator:          D. Herre
GitLab:    dikon/woma-cmif

Created:        2020-05-02
Last Modified:  2020-11-24
"""

import re
import json
from dateutil import parser
from datetime import datetime
from cmif import build, local  # , authority

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
# ~~ read json file with correspondence metadata and ~~ #
# ~~ authority data for persons and places ~~~~~~~~~~~~ #
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

metadata = []
with open("data/briefverzeichnis-online.json", 'r', encoding='utf-8') as f:
    metadata = json.load(f)

geonames = {}
with open("auth/geonames.json", 'r', encoding='utf-8') as f:
    geonames = json.load(f)

WOLFF_NAME = "Christian Wolff"
MANTEUFFEL_NAME = "Ernst Christoph von Manteuffel"
WOLFF_GND = "http://d-nb.info/gnd/118634771"
MANTEUFFEL_GND = "http://d-nb.info/gnd/118577352"

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
# ~~ directory path of folder for output json files ~~ #
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

OUT = "data"

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
# ~~ custom date parser for parsing german months ~~ #
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #


class MonthParser(parser.parserinfo):
    MONTHS = [
        ('JANUAR'),
        ('FEBRUAR'),
        ('MÄRZ'),
        ('APRIL'),
        ('MAI',),
        ('JUNI'),
        ('JULI'),
        ('AUGUST'),
        ('SEPTEMBER'),
        ('OKTOBER'),
        ('NOVEMBER'),
        ('DEZEMBER'),
    ]


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
# ~~ functions to process data of persons, places and dates ~~ #
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #


def person(name):
    """
    find GND number for person by given name
    return element <persName>
    """
    if name == "WOLFF":
        return build.tei_pers_name(WOLFF_NAME, WOLFF_GND)
    elif name == "MANTEUFFEL":
        return build.tei_pers_name(MANTEUFFEL_NAME, MANTEUFFEL_GND)
    else:
        print("could not identify person by name:", name + "!")
        return build.tei_pers_name("", "")


def place(name):
    """
    find GeoNames identifier for place given by name
    return element <placeName>
    """
    geoname_id = geonames[name.title()]
    return build.tei_place_name(
        name.title(),
        "http://www.geonames.org/" + geoname_id
        )


def isodate(date):
    """
    extract date from given line
    return element <date>
    """
    if re.match("(\\d\\d?\\.)/(\\d\\d?\\.)(\\s\\w+)(\\s\\d\\d\\d\\d)", date):
        groups = re.match(
          "(\\d\\d?\\.)/(\\d\\d?\\.)(\\s\\w+)(\\s\\d\\d\\d\\d)", date).groups()
        first_date = "{0}{1}{2}".format(groups[0], groups[2], groups[3])
        first_date = parser.parse(
            first_date, MonthParser()).strftime("%Y-%m-%d")
        last_date = "{0}{1}{2}".format(groups[1], groups[2], groups[3])
        last_date = parser.parse(
            last_date, MonthParser()).strftime("%Y-%m-%d")
        return build.tei_date(attrib_from=first_date,
                              attrib_to=last_date)
    try:
        # test if day is present by variant default values
        var1 = datetime.strptime('2001-01-01', '%Y-%m-%d')
        var2 = datetime.strptime('2002-02-02', '%Y-%m-%d')
        res1 = parser.parse(date, MonthParser(), default=var1).strftime("%Y-%m-%d")
        res2 = parser.parse(date, MonthParser(), default=var2).strftime("%Y-%m-%d")
        if res1 == res2:    # equal if no default value was used
            # if day is present return YYYY-MM-DD
            return build.tei_date(attrib_when=parser.parse(
                date, MonthParser()).strftime("%Y-%m-%d"))
        else:
            # if day is not present return YYYY-MM
            return build.tei_date(attrib_when=parser.parse(
                date, MonthParser()).strftime("%Y-%m"))
    except TypeError:
        if date == "30. AUGUST/1. SEPTEMBER 1747":
            return build.tei_date(attrib_from="1747-08-30",
                                  attrib_to="1747-09-01")
        elif date == "ENDE AUGUST/ANFANG SEPTEMBER 1740":
            return build.tei_date(attrib_not_before="1740-08",
                                  attrib_not_after="1740-09")
        elif date == "ENDE DEZEMBER 1740":
            return build.tei_date(attrib_not_before="1740-12",
                                  attrib_not_after="1740-12")
        elif date == "OHNE DATUM [FEBRUAR 1744]":
            return build.tei_date(attrib_not_before="1744-02",
                                  attrib_not_after="1744-02")
        elif date == "[1. ODER 2.] FEBRUAR 1745":
            return build.tei_date(attrib_not_before="1745-02-01",
                                  attrib_not_after="1745-02-02")
        elif date == "DEN 11. DEZEMBER":
            return build.tei_date(attrib_when="1746-12-11")
        elif date == "JUNI 1748 (FRÜHESTENS 24. JUNI":
            return build.tei_date(attrib_not_before="1748-06-24",
                                  attrib_not_after="1748-06-30")
        else:
            print("could not find date in given line:")
            print(date)
            return None

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
# ~~ main routine to create correspondence metadata file from plain texts ~~ #
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #


def main():
    """routine to create TEI-based file in CMI format"""
    # create <TEI>
    root = build.tei_root()
    # create <teiHeader>
    header = build.tei_header()
    # create <titleStmt>
    title = build.tei_title("Briefwechsel zwischen Christian Wolff und Ernst Christoph von Manteuffel 1738-1748")
    editor = build.tei_editor("Donatus Herre ")
    email = build.tei_email("donatus.herre@izea.uni-halle.de")
    editor.append(email)
    title_stmt = build.tei_title_stmt([title, editor])
    # create <publicationStmt>
    publisher = build.tei_publisher(
        build.tei_ref("Interdisziplinäres Zentrum für die Erforschung der Europäischen Aufklärung",
                      "https://www.izea.uni-halle.de"))
    url = "https://gitlab.informatik.uni-halle.de/dikon/woma-cmif/-/raw/master/data/briefverzeichnis-online.xml"
    idno = build.tei_idno(url)
    pub_date = build.tei_date(attrib_when=datetime.today().strftime("%Y-%m-%d"))
    availability = build.tei_availability(build.tei_license())
    pub_stmt = build.tei_publication_stmt([publisher, idno,
                                           pub_date, availability])
    # create <sourceDesc>
    bibl_bd1 = build.tei_bibl("Der Briefwechsel zwischen Christian Wolff und Ernst Christoph von Manteuffel 1738 bis 1748: Transkriptionen aus dem Handschriftenbestand der Universitätsbibliothek Leipzig. Hrsg. v. Katharina Middell und Hanns-Peter Neumann. Erster Teil – Briefe Nr. 1 bis 150 (11. Mai 1738 bis 30. Dezember 1743). Stand: Februar 2013. URN: ", attrib_type="online", domain="https://nbn-resolving.org/urn:nbn:de:bsz:14-qucosa-106446")
    ref = build.tei_ref("urn:nbn:de:bsz:14-qucosa-106446", "https://nbn-resolving.org/urn:nbn:de:bsz:14-qucosa-106446")
    bibl_bd1.append(ref)
    bibl_bd2 = build.tei_bibl("Der Briefwechsel zwischen Christian Wolff und Ernst Christoph von Manteuffel 1738 bis 1748: Transkriptionen aus dem Handschriftenbestand der Universitätsbibliothek Leipzig. Hrsg. v. Katharina Middell und Hanns-Peter Neumann. Zweiter Teil – Briefe Nr. 151 bis 314 (5. Januar 1744 bis 24. März 1747). Stand: Februar 2013. URN: ", attrib_type="online", domain="https://nbn-resolving.org/urn:nbn:de:bsz:14-qucosa-106452")
    ref = build.tei_ref("urn:nbn:de:bsz:14-qucosa-106452", "https://nbn-resolving.org/urn:nbn:de:bsz:14-qucosa-106452")
    bibl_bd2.append(ref)
    bibl_bd3 = build.tei_bibl("Der Briefwechsel zwischen Christian Wolff und Ernst Christoph von Manteuffel 1738 bis 1748: Transkriptionen aus dem Handschriftenbestand der Universitätsbibliothek Leipzig. Hrsg. v. Katharina Middell und Hanns-Peter Neumann. Dritter Teil – Briefe Nr. 315 bis 488 (26. März 1747 bis 5. November 1748). Stand: Februar 2013. URN: ", attrib_type="online", domain="https://nbn-resolving.org/urn:nbn:de:bsz:14-qucosa-106468")
    ref = build.tei_ref("urn:nbn:de:bsz:14-qucosa-106468", "https://nbn-resolving.org/urn:nbn:de:bsz:14-qucosa-106468")
    bibl_bd3.append(ref)
    source_desc = build.tei_source_desc([bibl_bd1, bibl_bd2, bibl_bd3])
    # create <fileDesc> with child nodes
    #   <titleStmt>, <publicationStmt> and <sourceDesc>
    file_desc = build.tei_file_desc([title_stmt, pub_stmt, source_desc])
    # add <fileDesc> to <teiHeader>
    header.append(file_desc)
    # create <profileDesc>
    profile_desc = build.tei_profile_desc()
    # loop over volumes
    for i in metadata["volume"]:
        volume = metadata["volume"][i]
        volume_bibl = source_desc[int(i)-1]
        # loop over letters
        for j in volume["letter"]:
            corresp = volume["letter"][j]
            # create <correspAction type="sent">
            author = person(corresp["author"])
            author_place = place(corresp["place"])
            author_date = isodate(corresp["date"])
            sent = build.tei_corresp_action("sent", children=[author,
                                                              author_place,
                                                              author_date])
            # create <correspAction type="received">
            addressee = person(corresp["addressee"])
            received = build.tei_corresp_action("received", children=[addressee])
            # create <correspDesc> with child nodes
            corresp_desc = build.tei_corresp_desc(
              attrib_key=j,
              attrib_source="#" + volume_bibl.get(build.ns_xml("id")),
              children=[sent, received])
            # add <correspDesc> to <profileDesc>
            profile_desc.append(corresp_desc)
    # add <profileDesc> to <teiHeader>
    header.append(profile_desc)
    # add <teiHeader> to <TEI>
    root.append(header)
    # create <text> element
    text = build.tei_text_empty()
    # add <text> to <TEI>
    root.append(text)
    # save <TEI> as XML
    local.writer(root, file="briefverzeichnis-online.xml", path=OUT)


if __name__ == '__main__':
    main()
