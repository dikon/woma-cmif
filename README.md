# Wolff-Manteuffel-Briefwechsel im CMI-Format

Dem von der [Deutsche Forschungsgemeinschaft](https://gepris.dfg.de/gepris/projekt/182659556) geförderten und gemeinsam vom [Interdisziplinären Zentrum für die Erforschung der Europäischen Aufklärung](https://www.izea.uni-halle.de/forschung/abgeschlossene-projekte/briefwechsel-wolff-manteuffel.html) und der [Sächsischen Akademie der Wissenschaften](https://www.saw-leipzig.de/de/projekte/historisch-kritische-edition-des-briefwechsels-zwischen-christian-wolff-und-ernst-christoph-graf-von-manteuffel) durchgeführten Projekt der „Historisch-kritischen Edition des Briefwechsels zwischen Christian Wolff und Ernst Christoph Graf von Manteuffel“ ist es nicht nur zu verdanken, dass die Korrespondenz zwischen dem Philosophen Wolff und seinem Förderer Manteuffel inzwischen gedruckt vorliegt:

> _Briefwechsel zwischen Christian Wolff und Ernst Christoph von Manteuffel 1738-1748_. Historisch-kritische Edition in 3 Bänden. Hrsg. v. Jürgen Stolzenberg, Detlef Döring, Katharina Middell und Hanns-Peter Neumann. Hildesheim; Zürich; New York: Olms, 2019.

Es sind auch die Transkriptionen, die der Edition zugrundeliegen, als „Open-Access Publikation“ über den sächsischen Dokumenten- und Publikationsserver Qucosa zugänglich:

> _Der Briefwechsel zwischen Christian Wolff und Ernst Christoph von Manteuffel 1738 bis 1748. Transkriptionen aus dem Handschriftenbestand der Universitätsbibliothek Leipzig_. Hrsg. v. Katharina Middell und Hanns-Peter Neumann. Stand: Februar 2013. URN: [urn:nbn:de:bsz:14-qucosa-106475](https://nbn-resolving.org/urn:nbn:de:bsz:14-qucosa-106475).

Unter Rückgriff auf die Transkriptionen sowie das Gesamtinhaltsverzeichnis der historisch-kritischen Edition wurden die Metadaten der Korrespondenz im [CMI-Format](https://correspsearch.net/en/participate.html) erfasst. Die resultierenden XML-Dateien, von denen eine in [correspSearch](https://correspsearch.net/en/search.html?c=https://gitlab.informatik.uni-halle.de/dikon/woma-cmif/-/raw/master/data/briefverzeichnis-print.xml) integriert wurde, sind [hier](./data/) zu finden.
